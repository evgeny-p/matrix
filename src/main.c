#include <stdio.h>

#include "display.h"

int main() {
    const useconds_t delay_usec = 50 * 1000;

    display_init();
    display_loop(delay_usec);
    display_restore();

    return 0;
}

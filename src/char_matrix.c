#include "char_matrix.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

CharMatrix *cm_new(size_t rows, size_t cols) {
    CharMatrix *cm = NULL;

    cm = malloc(sizeof(CharMatrix));

    if (!cm) {
        return NULL;
    }

    cm->cells = malloc(rows * cols);
    cm->rows = rows;
    cm->cols = cols;

    if (!cm->cells) {
        cm_free(cm);
        return NULL;
    }

    return cm;
}

void cm_free(CharMatrix *cm) {
    if (!cm) {
        return;
    }

    free(cm->cells);
    free(cm);
}

char cm_get(CharMatrix *cm, size_t i, size_t j) {
    assert(cm);
    assert(i < cm->rows && j < cm->cols);

    return cm->cells[i * cm->cols + j];
}

void cm_set(CharMatrix *cm, size_t i, size_t j, char value) {
    assert(cm);
    assert(i < cm->rows && j < cm->cols);

    cm->cells[i * cm->cols + j] = value;
}

void cm_clear(CharMatrix *cm) {
    assert(cm);

    memset(cm->cells, ' ', cm->rows * cm->cols);
}

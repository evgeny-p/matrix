#include "drop_matrix.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>

#define MIN(a, b) (((a) < (b)) ? (a) : (b))

static size_t get_drop_length(DropMatrix *dm) {
    return 1 + rand() % dm->char_matrix->rows;
}

static void dm_init(DropMatrix *dm) {
    assert(dm);

    size_t i;
    for (i = 0; i < dm->char_matrix->cols; ++i) {
        ColumnState *cs = &dm->column[i];
        cs->state = rand() % 2;
        cs->length = get_drop_length(dm);
    }

    cm_clear(dm->char_matrix);
}

DropMatrix *dm_new(size_t rows, size_t cols) {
    DropMatrix *dm = malloc(sizeof(DropMatrix));

    if (!dm) {
        return NULL;
    }

    dm->column = malloc(sizeof(ColumnState) * cols);
    dm->char_matrix = cm_new(rows, cols);
    dm->mask = malloc(rows * cols * sizeof(Mask));

    if (!dm->column || !dm->char_matrix || !dm->mask) {
        dm_free(dm);
        return NULL;
    }

    dm_init(dm);

    return dm;
}

void dm_free(DropMatrix *dm) {
    if (!dm) {
        return;
    }

    free(dm->column);
    free(dm->char_matrix);
    free(dm->mask);
    free(dm);
}

static char get_random_symbol() {
    return '!' + rand() % ('~' - '!');
}

static void dm_mask_set(DropMatrix *dm, size_t i, size_t j, Mask value) {
    assert(dm);
    assert(i < dm->char_matrix->rows && j < dm->char_matrix->cols);

    dm->mask[i * dm->char_matrix->cols + j] = value;
}

static void dm_shift_head(DropMatrix *dm) {
    size_t i;

    for (i = 0; i < dm->char_matrix->cols; ++i) {
        ColumnState *cs = &dm->column[i];

        if (cs->state == StateSpace) {
            cm_set(dm->char_matrix, 0, i, ' ');
        } else {
            const char current_symbol = cm_get(dm->char_matrix, 0, i);

            if (current_symbol == ' ') {
                cm_set(dm->char_matrix, 0, i, get_random_symbol());
                dm_mask_set(dm, 0, i, MaskCursor);
            }
        }

        assert(cs->length > 0);
        --cs->length;

        if (cs->length == 0) {
            cs->state = !cs->state;
            cs->length = get_drop_length(dm);
        }
    }
}

static void dm_shift_tail(DropMatrix *dm) {
    size_t i, j;

    for (i = dm->char_matrix->rows - 1; i > 0; --i) {
        for (j = 0; j < dm->char_matrix->cols; ++j) {
            const char upward_symbol = cm_get(dm->char_matrix, i - 1, j);
            const char current_symbol = cm_get(dm->char_matrix, i, j);

            if (upward_symbol == ' ') {
                cm_set(dm->char_matrix, i, j, ' ');
            } else if (current_symbol == ' ') {
                cm_set(dm->char_matrix, i, j, get_random_symbol());
                dm_mask_set(dm, i, j, MaskCursor);
            }
        }
    }
}

static void dm_mask_clean(DropMatrix *dm) {
    size_t i;
    const size_t item_count = dm->char_matrix->rows * dm->char_matrix->cols;

    for (i = 0; i < item_count; ++i) {
        dm->mask[i] = MaskSymbol;
    }
}

void dm_shift(DropMatrix *dm) {
    assert(dm);

    dm_mask_clean(dm);

    dm_shift_head(dm);
    dm_shift_tail(dm);
}

void dm_fill(DropMatrix *dm, DropMatrix *old_dm) {
    assert(dm);
    assert(old_dm);

    size_t i, j;

    const size_t common_cols
        = MIN(dm->char_matrix->cols, old_dm->char_matrix->cols);

    const size_t common_rows
        = MIN(dm->char_matrix->rows, old_dm->char_matrix->rows);

    for (j = 0; j < common_cols; ++j) {
        dm->column[j] = old_dm->column[j];
    }

    for (i = 0; i < common_rows; ++i) {
        for (j = 0; j < common_cols; ++j) {
            const char old_cm_item = cm_get(old_dm->char_matrix, i, j);
            cm_set(dm->char_matrix, i, j, old_cm_item);
        }
    }

    for (i = 0; i < common_rows; ++i) {
        for (j = 0; j < common_cols; ++j) {
            const Mask old_mask = dm_mask_get(old_dm, i, j);
            dm_mask_set(dm, i, j, old_mask);
        }
    }
}

Mask dm_mask_get(DropMatrix *dm, size_t i, size_t j) {
    assert(dm);
    assert(i < dm->char_matrix->rows && j < dm->char_matrix->cols);

    return dm->mask[i * dm->char_matrix->cols + j];
}

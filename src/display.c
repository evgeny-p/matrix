#include "display.h"

#include <signal.h>

#include <ncurses.h>

#include "drop_matrix.h"

#define UNUSED(x) (void)(x)

static int need_resize = 0;

static void resize_handler(int signum) {
    UNUSED(signum);
    need_resize = 1;
}

void display_init() {
    initscr();
    start_color();
    raw();
    noecho();
    cbreak();
    timeout(0);
    curs_set(0);

    init_pair(MaskSymbol, COLOR_GREEN, COLOR_BLACK);
    init_pair(MaskCursor, COLOR_WHITE, COLOR_BLACK);

    struct sigaction sa;
    sa.sa_handler = resize_handler;
    sigaction(SIGWINCH, &sa, NULL);
}

static void display_drop_matrix(DropMatrix *dm) {
    CharMatrix *cm = dm->char_matrix;

    move(0, 0);

    size_t i, j;
    for (i = 0; i < cm->rows; ++i) {
        for (j = 0; j < cm->cols; ++j) {
            const Mask mask = dm_mask_get(dm, i, j);

            attron(COLOR_PAIR(mask));
            addch(cm_get(cm, i, j));
            attroff(COLOR_PAIR(mask));
        }
    }

    refresh();
}

static DropMatrix *create_dm() {
    size_t rows, cols;

    getmaxyx(stdscr, rows, cols);
    DropMatrix *dm = dm_new(rows, cols);
    return dm;
}

static void handle_resize(DropMatrix **dm) {
    need_resize = 0;

    display_restore();
    refresh();
    display_init();

    DropMatrix *old_dm = *dm;
    *dm = create_dm();
    dm_fill(*dm, old_dm);
    dm_free(old_dm);
}

void display_loop(useconds_t delay_usec) {
    DropMatrix *dm = create_dm();

    while (1) {
        const char ch = getch();

        if (ch == 'q') {
            break;
        }

        display_drop_matrix(dm);
        dm_shift(dm);

        if (need_resize) {
            handle_resize(&dm);
        }

        usleep(delay_usec);
    }

    dm_free(dm);
}

void display_restore() {
    endwin();
}

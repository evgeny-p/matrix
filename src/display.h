#ifndef DISPLAY_H
#define DISPLAY_H

#include <unistd.h>

void display_init();
void display_loop(useconds_t delay_usec);
void display_restore();

#endif

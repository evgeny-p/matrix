#ifndef DROP_MATRIX
#define DROP_MATRIX

#include <stddef.h>

#include "char_matrix.h"

typedef struct {
    enum {
        StateSpace = 0,
        StateDrop = 1
    } state;
    size_t length;
} ColumnState;

typedef enum {
    MaskSymbol = 1,
    MaskCursor = 2
} Mask;

typedef struct {
    ColumnState *column;
    CharMatrix *char_matrix;
    Mask *mask;
} DropMatrix;

DropMatrix *dm_new(size_t rows, size_t cols);
void dm_free(DropMatrix *dm);

void dm_shift(DropMatrix *dm);
void dm_fill(DropMatrix *dm, DropMatrix *old_dm);

Mask dm_mask_get(DropMatrix *dm, size_t i, size_t j);

#endif

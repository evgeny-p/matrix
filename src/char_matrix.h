#ifndef CHAR_MATRIX_H
#define CHAR_MATRIX_H

#include <stddef.h>

typedef struct {
    char *cells;
    size_t rows;
    size_t cols;
} CharMatrix;

CharMatrix *cm_new(size_t rows, size_t cols);
void cm_free(CharMatrix *cm);

char cm_get(CharMatrix *cm, size_t i, size_t j);
void cm_set(CharMatrix *cm, size_t i, size_t j, char value);

void cm_clear(CharMatrix *cm);

#endif
